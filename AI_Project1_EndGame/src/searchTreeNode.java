
public class searchTreeNode {

	int ix, iy; // The (x,y) coordinates of Iron Man
	int damageLevel; // The damage level representation of Iron Man
	String stonesLoc; // The number of stones collected so far by Iron Man
	boolean isSnapped; // A flag to indicate whether Iron Man snapped or not yet
	
	String state = "";

	// To create a new search-tree node
	public searchTreeNode(int ix, int iy, int damageLevel, String liveWarriorLoc, String stonesLoc, boolean isSnapped) {

		this.ix = ix;
		this.iy = iy;
		this.damageLevel = damageLevel;
		this.stonesLoc = stonesLoc;
		this.isSnapped = isSnapped;
		
		//this.state = "grid("+ x +", " + y + ")"+"/n"+"ironman("+ix+", "+iy+")"+"/n"+ damageLevel + "; Live Warrior Locations: " + liveWarriorLoc + "; Stones Locations: " + stonesLoc + "; Snapped? " + isSnapped;

	}

}
//grid(5,5).
//ironman(1,2).
//thanos(3,4).
//
//stone1(1,1).
//stone2(2,1).
//stone3(2,2).
//stone4(3,3).
