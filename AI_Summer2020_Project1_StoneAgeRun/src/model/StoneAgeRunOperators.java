package model;

// An enumerator to help choosing one of agent's available operators
public enum StoneAgeRunOperators {
	
	MOVE_AGENT_RIGHT,
	MOVE_ROCK_RIGHT,
	MOVE_ROCK_LEFT,
	MOVE_ROCK_DOWN,
	MOVE_ROCK_UP
	
}