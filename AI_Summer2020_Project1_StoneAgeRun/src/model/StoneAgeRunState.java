package model;
import java.awt.Point;
import java.util.List;

// A data type for defining the State model
public class StoneAgeRunState {
	
	///////////////////////// Instance variables /////////////////////////

	// The agen't location on grid (consists of 2 cells Horizontally)
	private Point firstCell, secondCell;
	
	// List of the grid's rocks' locations
	private List<Rock> rocks;

	///////// Constructor /////////
	
	// State constructor
	public StoneAgeRunState(Point p1, Point p2, List<Rock> r) {
		
		firstCell = p1; secondCell = p2; rocks = r; }

	///////////////////////// Getter methods /////////////////////////
	
	// Getter method for the first cell in agent's location (out of 2)
	public Point getFirstCell() { return firstCell; }

	// Getter method for the second cell in agent's location (out of 2)
	public Point getSecondCell() { return secondCell; }
	
	// Getter method for the list of grid's rocks at this state
	public List<Rock> getRocks() { return rocks; }

	///////// Helper methods /////////

	// Helper method that overrides the Object's equals() method to match two states
	public boolean equals(Object o) {

		if (o instanceof StoneAgeRunState)
			if (this.firstCell.equals(((StoneAgeRunState) o).getFirstCell()) &&
					this.secondCell.equals(((StoneAgeRunState) o).getSecondCell()))
				return true;
		return false;

	}

	// Helper method that overrides the String's toString() to display state data
	public String toString() {

		return "<" + getFirstCell().toString() + ", " + getSecondCell().toString() +
				", " + rocks.toString() + ">";

	}

	/////////////////////////
	
}