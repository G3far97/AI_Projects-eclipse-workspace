package model;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import search.SearchStrategy;

// Subclass of the generic search problem.
public class StoneAgeRun extends SearchProblem {

	///////////////////////// Instance variables /////////////////////////

	private Collection<Object> operators; // Set of actions available to the agent.
	private StoneAgeRunState initialState, goalState; // Agent Start/Goal state.
	private List<List<Object>> stateSpace; // The set of states: initialState -> state.
	private Object[][] grid; // The problem's generated grid.
	private List<Rock> rocksArray;
	
	///////// Constructor /////////

	// Problem constructor
	public StoneAgeRun()  { operatorsSetup(); GenGrid(); }

	///////// operatorsSetup() helper method /////////

	// Helper method to setup operators' ArrayList
	private void operatorsSetup() {

		operators = new ArrayList<Object>();
		operators.add(StoneAgeRunOperators.MOVE_AGENT_RIGHT);
		operators.add(StoneAgeRunOperators.MOVE_ROCK_DOWN);
		operators.add(StoneAgeRunOperators.MOVE_ROCK_LEFT);
		operators.add(StoneAgeRunOperators.MOVE_ROCK_RIGHT);
		operators.add(StoneAgeRunOperators.MOVE_ROCK_UP);

	}

	///////////////////////// GenGrid() method /////////////////////////

	/* generates a random grid. The dimensions of the grid, the initial location
	of the agent in the second row, as well as the locations and orientations of the rocks
	are to be randomly generated. */
	private void GenGrid() {

		int min = 4, dimMax = 10,
				rand1 = (int) (Math.random() * dimMax) + min,
				rand2 = (int) (Math.random() * dimMax) + min,
				agentYRange = rand2 - min + 1,
				agentY = (int) (Math.random() * agentYRange);

		// Generating a random dimensions 2D grid
		grid = new Object[rand1][rand2];
		
		// Initial location of the agent in the second row
		Point initialStateFirstCell = new Point(1, agentY),
				initialStateSecondCell = new Point(1, agentY + 1);
		
		// Goal state location
		Point goalStatePoint = new Point(1, rand2 - 2);

		// Rocks generating
		List<Rock> rocks = rocksGenerator(rand1, rand2,
						initialStateFirstCell, initialStateSecondCell);

//		// State space filling
//		stateSpaceFiller(rand1, rand2,
//				initialStateFirstCell, initialStateSecondCell, rocks);

		// Map the initial grid state to the actual grid
		initialGridMapper(initialStateFirstCell, initialStateSecondCell,
				goalStatePoint, rocks);

	}

	///////// GenGrid() helper methods /////////

	// Helper method to setup the stateSpace's ArrayList 
//	private void stateSpaceFiller
//	(int rand1, int rand2, Point initP1, Point initP2, List<Rock> rocks) {
//
//		// 
//		stateSpace = new ArrayList<List<Object>>();
//		
//		// 
//		stateSpace.add(ArrayList<StoneAgeRunState>());
//		
////		for (List<Object> ss1 : stateSpace) {
////
////			stateSpace.add(new ArrayList<Object>());
////
////			stateSpace.get(i).add
////			(new StoneAgeRunState(new Point(1, i), new Point(1, i + 1)));
////
////			stateSpace.get(i).add
////			(new StoneAgeRunState(new Point(1, i + 1), new Point(1, i + 2)));
////
////		}
////
////		stateSpace.add(new ArrayList<Object>());
////
////		stateSpace.get(stateSpace.size()-1).add
////		(new StoneAgeRunState(new Point(1, rand2 - 2), new Point(1, rand2 - 1)));
//
//	}

	// Helper method to generate rocks (random dimensions, locations and orientations)
	private List<Rock> rocksGenerator(int rand1, int rand2, Point initP1, Point initP2) {

		RockOrientation rockOrientation;
		int rockSize, p1Max,
		nRocksMax = 2 , nRocks = (int) (Math.random() * nRocksMax) + 1;
		Point p1Cell, p2Cell, p3Cell;
		Rock rock;
		ArrayList<Rock> rocksArray = new ArrayList<Rock>();

		while(nRocks > 0) {

			rockOrientation = (int) (Math.random() * 2) == 0 ?
					RockOrientation.H : RockOrientation.V;

			rockSize = (int) (Math.random() * 2) + 2;

			p1Max = (rockOrientation.equals(RockOrientation.H) ?
					rand2 - (rockSize == 2 ? 2 : 3) : rand1 - (rockSize == 2 ? 2 : 3));

			p1Cell = new Point(rockOrientation.equals(RockOrientation.H) ?
					(int) Math.random() * rand1 : (int) Math.random() * p1Max,
					rockOrientation.equals(RockOrientation.V) ?
							(int) Math.random() * rand2 : (int) Math.random() * p1Max);

			p2Cell = rockOrientation.equals(RockOrientation.H) ?
					new Point(p1Cell.x, p1Cell.y + 1) : new Point(p1Cell.x + 1, p1Cell.y);

			p3Cell = rockSize == 3 ? rockOrientation.equals(RockOrientation.H) ?
					new Point(p1Cell.x, p1Cell.y + 2) :
						new Point(p1Cell.x + 2, p1Cell.y) : null;

			rock = rockSize == 2 ? new Rock(rockOrientation, p1Cell, p2Cell) :
				new Rock(rockOrientation, p1Cell, p2Cell, p3Cell);

			if (!rocksArray.contains(rock) && !isAgentConflict(rock, initP1, initP2)) {

				rocksArray.add(rock); nRocks--;

			}

		}
		
		this.rocksArray = rocksArray;
		return rocksArray;

	}

	// Helper method to detect if the generated rock conflicts agent's location
	private boolean isAgentConflict(Rock r, Point initP1, Point initP2) {

		if(r.getFirstCell().equals(initP1) ||
				r.getFirstCell().equals(initP2) ||
				r.getSecondCell().equals(initP1) ||
				r.getSecondCell().equals(initP2) ||
				r.getThirdCell().equals(initP1) ||
				r.getThirdCell().equals(initP2))
			return true;
		
		return false;

	}
	
	// Helper method to map the initial grid state
	private void initialGridMapper
	(Point initP1, Point initP2, Point goal, List<Rock> rocks) {
		
		// Declare and initialize the new states
		StoneAgeRunState state = new StoneAgeRunState(initP1, initP2, rocks);
		StoneAgeRunState goalState = new StoneAgeRunState(goal, goal, rocks);
		
		// Map the agent on the grid
		grid[initP1.x][initP2.y] = state;
		grid[initP2.x][initP2.y] = state;
		
		// Map the goal state on the grid
		this.goalState = goalState;
		
		// Map the array of rocks on the grid
		for(Rock r : rocks) {
			
			grid[r.getFirstCell().x][r.getFirstCell().y] = r;
			grid[r.getSecondCell().x][r.getSecondCell().y] = r;
			if(!r.getThirdCell().equals(null))
				grid[r.getFirstCell().x][r.getFirstCell().y] = r;
			
		}
		
	}

	///////////////////////// Goal test & path cost methods /////////////////////////

	// Goal test function, applied to stop the agent when reaching its goal
	public boolean isGoal(Object node) {

		if(node instanceof SearchTreeNode)
			if ((((SearchTreeNode) node).getState()).equals(this.getGoalState()))
				return true;
		
		return false;

	}

	// Path cost function, the sum of the costs of individual actions in the sequence.
	public double pathCost(SearchTreeNode node) { return node.getPathCost(); }

	///////////////////////// Getters & Setters methods /////////////////////////

	// Getter method for the set of actions available to the agent
	public Collection<Object> getOperators() { return operators; }

	// Getter method for the agent's start state
	public StoneAgeRunState getInitialState() { return initialState; }

	// Getter method for the agent's end state
	public StoneAgeRunState getGoalState() { return goalState; }

	// Getter method for the state space
	public List<List<Object>> getStateSpace() { return stateSpace; }

	// Getter method for the problem's generated grid
	public Object[][] getGrid() { return grid; }

	// Setter method for the problem's grid
	public void setGrid(Object[][] grid) { this.grid = grid; }

	///////////////////////// Next state method /////////////////////////

	// A method to get the next state in the state space by applying an operator
	public Object getNextState(Object state, Object operator) {

		// Set up required variables
		int agentEndX = ((StoneAgeRunState) state).getSecondCell().x,
				agentEndY = ((StoneAgeRunState) state).getSecondCell().y;
		
		// Declare and initialize agent's second and next (potential) cells
		Point agentEndPoint = ((StoneAgeRunState) state).getSecondCell(),
				agentNextPoint1 = new Point(agentEndX, agentEndY + 1),
				agentNextPoint2 = new Point(agentEndX, agentEndY + 2),
				agentNextPoint3 = new Point(agentEndX, agentEndY + 3);
		
		// Set up variables to save rock orientations
		RockOrientation hRock = RockOrientation.H, vRock = RockOrientation.V;
		
		// Get the rocks array from the given state
		List<Rock> rocksArray = ((StoneAgeRunState) state).getRocks();
		
		// 
		Rock potentialRock1 = rocksArray.contains(
				new Rock(hRock, agentNextPoint1, agentNextPoint2)) ?
						new Rock(hRock, agentNextPoint1, agentNextPoint2) : null;
		
		// 
		Rock potentialRock2 = rocksArray.contains(
				new Rock(vRock, agentNextPoint1, agentNextPoint2)) ?
						new Rock(vRock, agentNextPoint1, agentNextPoint2) : null;
		
		// 
		Rock potentialRock3 = rocksArray.contains(
				new Rock(hRock, agentNextPoint1, agentNextPoint2, agentNextPoint3)) ?
						new Rock(hRock, agentNextPoint1, agentNextPoint2,
								agentNextPoint3) : null;
		
		// 
		Rock potentialRock4 = rocksArray.contains(
				new Rock(vRock, agentNextPoint1, agentNextPoint2, agentNextPoint3)) ?
						new Rock(vRock, agentNextPoint1, agentNextPoint2,
								agentNextPoint3) : null;

		// 
		Rock rock = potentialRock1.equals(null) ? potentialRock2.equals(null) ?
				potentialRock3.equals(null) ? potentialRock4.equals(null) ?
						null : potentialRock4 : potentialRock3 :
							potentialRock2 : potentialRock1;
		
		// Based on the operator, determine the "next state"
		if(operator instanceof StoneAgeRunOperators)
			switch((StoneAgeRunOperators) operator) {
			case MOVE_AGENT_RIGHT: return rock.equals(null) ?
					new StoneAgeRunState(agentEndPoint, agentNextPoint1,
							((StoneAgeRunState) state).getRocks()) : state;
			case MOVE_ROCK_RIGHT: case MOVE_ROCK_LEFT:
				case MOVE_ROCK_DOWN: case MOVE_ROCK_UP: return moveRock(rock); 
			}

		return null; // Returns null in default cases

	}

	///////// moveRock() Helper method /////////

	// Helper method to to move the rock away from the agent
	private StoneAgeRunState moveRock(Rock rock) {

		// Set up required variables to get agent coordinates
		int agentStartX = initialState.getFirstCell().x;
		int agentStartY = initialState.getFirstCell().y;
		//int agentEndX = initialState.getSecondCell().x;
		int agentEndY = initialState.getSecondCell().y;
		
		// Set up required variables to get rock coordinates
		int rockStartX = rock.getFirstCell().x;
		int rockStartY = rock.getFirstCell().y;
		int rockEndX = rock.getRockState()[rock.getRockState().length - 1].x;
		int rockEndY = rock.getRockState()[rock.getRockState().length - 1].y;
		
		// Set up required variables to determine rock orientation
		boolean isHorizontal = rock.getRockOriantation().equals(RockOrientation.H);
		boolean isVertical = rock.getRockOriantation().equals(RockOrientation.V);
		
		// Set up required variables to determine rock position
		boolean inLine = (agentStartX == rockEndX) && isHorizontal;
		boolean backRock = (agentStartY > rockEndY) && isHorizontal;
		boolean frontRock = (agentEndY < rockStartY) && isHorizontal;
		
		// 
		boolean agentSpaceLeft = (agentStartY > rockEndY + 1) && isHorizontal;
		boolean rockSpaceLeft = (rockStartY > 0) && isHorizontal;
		boolean rockSpaceRight = (rockEndY < grid[rockEndX].length) && isHorizontal;
		boolean rockSpaceUp = (rockStartX > 0) && isVertical;
		boolean rockSpaceDown = (rockEndX > grid.length) && isVertical;
		
		//
		Rock balabizo = null, newRock;
		
		// 
		if(isHorizontal) {
			
			if(inLine) {
				
				if(backRock) {

					if(agentSpaceLeft && rockSpaceRight){

						for(int i = rockEndY + 1; i < agentStartY; i++)
							if(rocksArray.contains
									(new Rock(RockOrientation.H,
											new Point(1, i), new Point(1, i + 1)))) {
												
								balabizo = new Rock(RockOrientation.H,
										new Point(1, i), new Point(1, i + 1));
							
							}
							else if(rocksArray.contains
									(new Rock(RockOrientation.V,
											new Point(1, i), new Point(1, i + 1)))) {
								
								balabizo = new Rock(RockOrientation.V,
										new Point(1, i), new Point(1, i + 1));
							
							}

					}
					else if(rockSpaceLeft) {
						
						for(int i = rockStartY - 1; i > 0; i--)
							if(rocksArray.contains
									(new Rock(RockOrientation.H,
											new Point(1, i), new Point(1, i + 1)))) {
								
								balabizo = new Rock(RockOrientation.H,
										new Point(1, i), new Point(1, i + 1));
							
							}
							else if(rocksArray.contains
									(new Rock(RockOrientation.V,
											new Point(1, i), new Point(1, i + 1)))) {
								
								balabizo = new Rock(RockOrientation.V,
										new Point(1, i), new Point(1, i + 1));
							
							}
						
					}
					
				} else if (frontRock) return null;
				
			}
			else if(rockSpaceRight) {
				
				for(int i = rockEndY + 1; i < grid[1].length; i++)
					if(rocksArray.contains
							(new Rock(RockOrientation.H,
									new Point(1, i), new Point(1, i + 1)))) {
						
						balabizo = new Rock(RockOrientation.H,
								new Point(1, i), new Point(1, i + 1));
					
					}
					else if(rocksArray.contains
							(new Rock(RockOrientation.V,
									new Point(1, i), new Point(1, i + 1)))) {
						
						balabizo = new Rock(RockOrientation.V,
								new Point(1, i), new Point(1, i + 1));
					
					}
				
			}
			else if(rockSpaceLeft) {
				
				for(int i = rockStartY - 1; i > 0; i--)
					if(rocksArray.contains
							(new Rock(RockOrientation.H,
									new Point(1, i), new Point(1, i + 1)))) {
						
						balabizo = new Rock(RockOrientation.H,
								new Point(1, i), new Point(1, i + 1));
					
					}
					else if(rocksArray.contains
							(new Rock(RockOrientation.V,
									new Point(1, i), new Point(1, i + 1)))) {
						
						balabizo = new Rock(RockOrientation.V,
								new Point(1, i), new Point(1, i + 1));
					
					}
				
			}
			
		}
		else if(isVertical) {
			
			if(rockSpaceDown) {
				
				for(int i = rockEndX + 1; i < grid.length; i++)
					if(rocksArray.contains
							(new Rock(RockOrientation.H,
									new Point(i, rockEndY), new Point(i + 1, rockEndY)))) {
						
						balabizo = new Rock(RockOrientation.H,
								new Point(i, rockEndY), new Point(i + 1, rockEndY));
					
					}
					else if(rocksArray.contains
							(new Rock(RockOrientation.V,
									new Point(i, rockEndY), new Point(i + 1, rockEndY)))) {
						
						balabizo = new Rock(RockOrientation.V,
								new Point(i, rockEndY), new Point(i + 1, rockEndY));
					
					}
				
			}
			else if(rockSpaceUp) {
				
				for(int i = rockStartX - 1; i > 0; i--)
					if(rocksArray.contains
							(new Rock(RockOrientation.H,
									new Point(i, rockStartY),
									new Point(i + 1, rockStartY)))) {
						
						balabizo = new Rock(RockOrientation.H,
								new Point(i, rockStartY), new Point(i + 1, rockStartY));
					
					}
					else if(rocksArray.contains
							(new Rock(RockOrientation.V,
									new Point(i, rockStartY),
									new Point(i + 1, rockStartY)))) {
						
						balabizo = new Rock(RockOrientation.V,
								new Point(i, rockStartY), new Point(i + 1, rockStartY));
					
					}
				
			}
		
		}
		
		if(!balabizo.equals(null)) return moveRock(balabizo);
		
		return new StoneAgeRunState
				(initialState.getFirstCell(), initialState.getSecondCell(), this.rocksArray);

	}

	///////////////////////// Search method (solver) /////////////////////////

	// Uses search to try to formulate a winning plan
	public List<Object> Search
	(Object [][] grid, SearchStrategy strategy, boolean visualize) {

		// Declare and initialize the output
		List<Object> result = new ArrayList<Object>();

		this.setGrid(grid); // Assigning the given grid to the problem

		// Declare and initialize the expansion output
		List<SearchTreeNode> expansion = new ArrayList<SearchTreeNode>();

		// Based on the search strategy, generate the expansion sequence
		switch(strategy) {
		case BF: expansion.addAll(search.Search.bfSearch((SearchProblem)this)); break;
		case DF: expansion.addAll(search.Search.dfSearch((SearchProblem)this)); break;
		case ID: expansion.addAll(search.Search.idSearch((SearchProblem)this)); break;
		case UC: expansion.addAll(search.Search.ucSearch((SearchProblem)this)); break;
		case GR1: expansion.addAll(search.Search.greedySearch1((SearchProblem)this)); break;
		case GR2: expansion.addAll(search.Search.greedySearch2((SearchProblem)this)); break;
		case AS1: expansion.addAll(search.Search.aStarSearch1((SearchProblem)this)); break;
		case AS2: expansion.addAll(search.Search.aStarSearch2((SearchProblem)this)); break;
		}

		// Representation of the sequence of moves to reach the goal
		String solution = expansion.get(expansion.size()-1).pathToString();

		// The cost of the solution computed
		double solutionCost = 0;
		for(SearchTreeNode node : expansion.get(expansion.size()-1).getPathFromRoot())
			solutionCost += node.getPathCost();

		// The number of nodes chosen for expansion during the search
		int expandedNodes = expansion.size();

		// Set up the resulting ArrayList
		result.add(solution); result.add(solutionCost); result.add(expandedNodes);

		return result; // Returns the resulting ArrayList

	}

}