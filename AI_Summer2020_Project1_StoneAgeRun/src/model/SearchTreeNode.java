package model;

import java.util.ArrayList;
import java.util.List;

// A data type for a search-tree node
public class SearchTreeNode {

	/////////////////////////
	
	/* Following Russell and Norvig, we shall consider nodes to be 5-tuples:
	 * The @state of the state space that this node corresponds to.
	 * The @parent node.
	 * The @operator applied to generate this node.
	 * The @depth of the node in the tree.
	 * The @pathCost from the root.
	 */
	
	// @heuristic1: An attribute to store the first heuristic function's value (if exists)
	// @heuristic2: An attribute to store the second heuristic function's value (if exists)
	
	///////////////////////// Instance variables /////////////////////////

	private Object state, operator;
	private SearchTreeNode parent;
	private int depth, heuristic1, heuristic2;
	private double pathCost;
	
	///////// Constructors /////////

	// Constructor for default search-tree node
	public SearchTreeNode(Object s) { this.state = s; }
	
	// Constructor for a child search-tree node with heuristic function's values
	public SearchTreeNode(Object s, SearchTreeNode p, Object o, double c, int h1, int h2) {
		
		this(s);
		this.parent = p;
		this.operator = o;
		this.pathCost = this.parent.pathCost + c;
		this.heuristic1 = h1;
		this.heuristic2 = h2;
		
	}

	///////////////////////// Getter methods /////////////////////////
	
	// Getter method for the search-tree node's state
	public Object getState() { return state; }

	// Getter method for the search-tree node's parent search-tree node
	public SearchTreeNode getParent() { return parent; }

	// Getter method for the operator that generated the search-tree node
	public Object getOperator() { return operator; }

	// Getter method for search-tree node's depth
	public int getDepth() { depth = isRoot() ? 0 : 1 + parent.getDepth(); return depth;}

	// Getter method for search-tree node's total cost from the root
	public double getPathCost() { return pathCost; }

	// Getter method for search-tree node's first heuristic function's value
	public int getHeuristic1() { return heuristic1; }
	
	// Getter method for search-tree node's second heuristic function's value
	public int getHeuristic2() { return heuristic2; }
	
	///////////////////////// Helper methods /////////////////////////

	// Helper method to check if the current search-tree node is the tree's root
	public boolean isRoot() { return parent == null; }

	// Helper method to override the default toString() method to display the node's data
	public String toString() {
		return "[state= " + getState() + ", parent= " + getParent() + ", operator= "
				+ getOperator() + ", pathCost= " + getPathCost() + ", heuristic1= " +
				getHeuristic1() + ", heuristic2= " + getHeuristic2() + "]";
	}

	// Helper method to return the node's path from the root
	public List<SearchTreeNode> getPathFromRoot() {
		List<SearchTreeNode> path = new ArrayList<SearchTreeNode>();
		SearchTreeNode current = this;
		while (!current.isRoot()) {
			path.add(0, current);
			current = current.getParent();
		}
		path.add(0, current);
		return path;
	}

	// Helper method to represent the search-tree node's path from root
	public String pathToString() {
		String s = "";
		List<SearchTreeNode> nodes = getPathFromRoot();
		for (int i = 0; i < nodes.size(); i++)
			System.out.print("Operator: " + nodes.get(i).getOperator() +
					" -> " + "State: " + nodes.get(i).getState());
		return s;
	}

	/////////////////////////

}