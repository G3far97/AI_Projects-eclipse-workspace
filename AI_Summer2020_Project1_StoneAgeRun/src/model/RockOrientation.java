package model;

// An enumerator to help choosing the rock's orientation
public enum RockOrientation { H, V }