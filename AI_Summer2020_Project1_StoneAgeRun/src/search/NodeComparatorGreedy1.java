package search;
import java.util.Comparator;

import model.SearchTreeNode;

// A comparator class to apply Greedy Search Algorithm on a priority queue
public class NodeComparatorGreedy1 implements Comparator<SearchTreeNode> {

	@Override // To abide the algorithm while inserting in the queue
	public int compare(SearchTreeNode n1, SearchTreeNode n2) {
		
		// Comparing nodes based on their heuristic function's values
		if(n1.getHeuristic1() >= n2.getHeuristic1()) return 1;
		else return -1;
	
	}

}