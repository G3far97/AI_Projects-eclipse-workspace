package search;

// An enumerator to determine which queuing method to follow
public enum QingFunction {
	
	ENQUEUE_AT_END,
	ENQUEUE_AT_FRONT,
	ORDERED_INSERT_UCS,
	BEST_FIRST_GREEDY1,
	BEST_FIRST_GREEDY2,
	BEST_FIRST_ASTAR1,
	BEST_FIRST_ASTAR2

}