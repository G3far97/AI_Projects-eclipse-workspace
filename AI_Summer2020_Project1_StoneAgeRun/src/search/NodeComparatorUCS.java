package search;
import java.util.Comparator;

import model.SearchTreeNode;

// A comparator class to apply Uniform Cost Search Algorithm on a priority queue
public class NodeComparatorUCS implements Comparator<SearchTreeNode> {

	@Override // To abide the algorithm while inserting in the queue
	public int compare(SearchTreeNode n1, SearchTreeNode n2) {
		
		// Comparing nodes based on their path cost function's values
		if(n1.getPathCost() >= n2.getPathCost()) return 1;
		else return -1;
		
	}

}