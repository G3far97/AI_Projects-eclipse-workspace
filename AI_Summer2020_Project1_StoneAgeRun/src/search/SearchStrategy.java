package search;

// An enumerator to help choosing the strategy followed to solve the problem
public enum SearchStrategy { BF, DF, ID, UC, GR1, GR2, AS1, AS2 }