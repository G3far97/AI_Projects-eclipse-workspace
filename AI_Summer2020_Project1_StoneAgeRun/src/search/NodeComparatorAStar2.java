package search;
import java.util.Comparator;

import model.SearchTreeNode;

// A comparator class to apply A* SearchAlgorithm on a priority queue
public class NodeComparatorAStar2 implements Comparator<SearchTreeNode> {
	
	@Override // To abide the algorithm while inserting in the queue
	public int compare(SearchTreeNode n1, SearchTreeNode n2) {
		
		// Comparing nodes based on their (heuristic + path cost) functions' values
		if(n1.getHeuristic2()+n1.getPathCost() >= n2.getHeuristic2()+n2.getPathCost())
			return 1;
		else return -1;
	
	}
	
}