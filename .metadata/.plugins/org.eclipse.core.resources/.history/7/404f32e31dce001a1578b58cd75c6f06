package model;
import java.util.Collection;
import java.util.List;

// An abstract data type for a generic search problem
public abstract class SearchProblem {
	
	/////////////////////////
	
	// A problem is defined as a 5-tuple:
	
	///////////////////////// Instance variables /////////////////////////
	
	private Collection<Object> operators; // Set of actions available to the agent.
	
	private Object initialState; // Agent's start state.
	
	// The set of states reachable from the initial state by any sequence of actions.
	private List<List<Object>> stateSpace;
	
	///////////////////////// Goal test & path cost methods /////////////////////////
	
	// A goal test, which the agent applies to a state to determine if it is a goal state.
	public abstract boolean isGoal(Object state);
	
	// A function that assigns cost to a sequence of actions.
	public abstract double pathCost(SearchTreeNode node);
	
	///////////////////////// Getter methods /////////////////////////
	
	// Getter method for the set of operators
	public Collection<Object> getOperators() { return operators; }
	
	// Getter method for the agent's start state
	public Object getInitialState() { return initialState; }
	
	// Getter method for the state space
	public List<List<Object>> getStateSpace() { return stateSpace; }
	
	// Setter method for the state space
	public void setStateSpace(List<List<Object>> ss) { this.stateSpace = ss; }
	
	/////////////////////////
	
	
}