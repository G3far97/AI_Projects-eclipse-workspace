package model;
import java.awt.Point;

import RockOrientation;

// A data type to represent a rock with it's size, location and orientation
public class Rock {

	/////////////////////////

	private RockOrientation rockOriantation; // Horizontal or vertical rock ?
	private Point firstCell, secondCell, thirdCell; // Rock location

	/////////////////////////

	// Rock constructor (rock size = 2)
	public Rock(RockOrientation ro, Point p1, Point p2) {

		this.rockOriantation = ro;
		this.firstCell = p1;
		this.secondCell = p2;

	}

	// Rock constructor (rock size = 3)
	public Rock(RockOrientation ro, Point p1, Point p2, Point p3) {

		this(ro, p1, p2);
		this.thirdCell = p3;

	}

	/////////////////////////

	// Getter method for rock's orientation (Horizontal or Vertical)
	public RockOrientation getRockOriantation() { return rockOriantation; }

	// Getter method for rock's first cell
	public Point getFirstCell() { return firstCell; }

	// Getter method for rock's second cell
	public Point getSecondCell() { return secondCell; }

	// Getter method for rock's third cell
	public Point getThirdCell() { return thirdCell; }

	/////////////////////////

	// Helper method to return the whole state of the rock (first, second and third cells)
	public Point[] getRockState() {

		Point[] rockState2D = {firstCell, secondCell};
		Point[] rockState3D = {firstCell, secondCell, thirdCell};

		return thirdCell.equals(null) ? rockState2D : rockState3D;

	}

	// Helper method to override the Object's equals() to match rocks
	public boolean equals(Object r) {
		
		if (r instanceof Rock) 
			return this.getRockOriantation().equals(((Rock) r).getRockOriantation()) &&
					this.getFirstCell().equals(((Rock) r).getFirstCell()) &&
					this.getSecondCell().equals(((Rock) r).getSecondCell()) &&
					this.getThirdCell().equals(((Rock) r).getThirdCell());
		return false;
		
	}
	
	/////////////////////////

}