package search;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

import model.SearchProblem;
import model.SearchTreeNode;

// 
public abstract class Search {

	/////////////////////////

	// The generic search algorithm, taking a problem and a search strategy as inputs.
	public static Collection<SearchTreeNode> generalSearch
	(SearchProblem problem, QingFunction qing) {

		// Declaring and initializing the expansion storage queue
		Collection<SearchTreeNode> nodes = destCreator(problem, qing);

		// Loop till you reach to the solution or return a failure
		while(!nodes.isEmpty()) {

			// Extract the first node from the storage queue
			SearchTreeNode node = nodeRemoval(nodes);

			// If this node is the goal state, congratulations!
			if (problem.isGoal((SearchTreeNode) node.getState())) {

				nodeAdder(nodes, node); // Add it again at the front of the storage queue
				return nodes; // Returns the expansion queue (with goal state at top)

			} else {

				// Wasn't the goal state ? expand it, put it back and try again!
				nodes = qinFun(nodes, expand(node, problem.getOperators()), qing);
			}
		}

		return nodes; // 
	}

	/////////

	// 
	private static Collection<SearchTreeNode> destCreator(SearchProblem p, QingFunction q) {

		// 
		Collection<SearchTreeNode> dest = null;

		// 
		switch(q) {
		case ENQUEUE_AT_END: dest = new LinkedList<SearchTreeNode>(); break;
		case ENQUEUE_AT_FRONT: dest = new Stack<SearchTreeNode>(); break;
		case ORDERED_INSERT_UCS: dest =
				new PriorityQueue<SearchTreeNode>(new NodeComparatorUCS()); break;
		case BEST_FIRST_GREEDY: dest =
				new PriorityQueue<SearchTreeNode>(new NodeComparatorGreedy()); break;
		case BEST_FIRST_ASTAR: dest =
				new PriorityQueue<SearchTreeNode>(new NodeComparatorAStar()); break;
		default: dest = null;
		}

		// 
		dest.add(new SearchTreeNode(p.getInitialState()));

		return dest; // 

	}

	// 
	private static SearchTreeNode nodeRemoval(Collection<SearchTreeNode> nodes) {

		// 
		SearchTreeNode node = null;
		if(nodes instanceof Queue<?>) // 
			node = ((Queue<SearchTreeNode>) nodes).poll();
		if(nodes instanceof Stack<?>) // 
			node = ((Stack<SearchTreeNode>) nodes).pop();
		return node; // 

	}
	
	// 
	private static void nodeAdder(Collection<SearchTreeNode> nodes, SearchTreeNode n) {
		
		if(nodes instanceof LinkedList<?>) // 
			((LinkedList<SearchTreeNode>) nodes).add(0, n);
		if(nodes instanceof PriorityQueue<?>) // 
			((PriorityQueue<SearchTreeNode>) nodes).add(n);
		if(nodes instanceof Stack<?>) // 
			((Stack<SearchTreeNode>) nodes).push(n);
		
	}

	// 
	private static Collection<SearchTreeNode> expand
	(SearchTreeNode node, Collection<Object> operators) {

		// 
		Collection<SearchTreeNode> expanded = new ArrayList<SearchTreeNode>();

		// 
		//for(Object o : operators) {  }
		return expanded; // 

	}

	// 
	private static Collection<SearchTreeNode> qinFun
	(Collection<SearchTreeNode> dest, Collection<SearchTreeNode> src, QingFunction q) {

		// 
		switch(q) {
		case ENQUEUE_AT_END: dest.addAll(src); break;
		case ENQUEUE_AT_FRONT: for(Object e : src)
		{ ((Stack<SearchTreeNode>) dest).push((SearchTreeNode) e); } break;
		case ORDERED_INSERT_UCS:
		case BEST_FIRST_GREEDY:
		case BEST_FIRST_ASTAR: for(Object e : src) { dest.add((SearchTreeNode) e); } break;
		default: dest.add(null);
		}

		// 
		return dest;

	}

	/////////////////////////

	// 
	public static LinkedList<SearchTreeNode> bfSearch(SearchProblem problem) {

		// 
		return (LinkedList<SearchTreeNode>)
				generalSearch(problem, QingFunction.ENQUEUE_AT_END);

	}

	/////////

	// 
	public static PriorityQueue<SearchTreeNode> ucSearch(SearchProblem problem) {

		// 
		return (PriorityQueue<SearchTreeNode>)
				generalSearch(problem, QingFunction.ORDERED_INSERT_UCS);

	}

	/////////

	// 
	public static Stack<SearchTreeNode> dfSearch(SearchProblem problem) {

		// 
		return (Stack<SearchTreeNode>)
				generalSearch(problem, QingFunction.ENQUEUE_AT_FRONT);

	}

	/////////

	// 
	public static Stack<SearchTreeNode> dlSearch(SearchProblem problem, int depth) {

		// 
		problem.getStateSpace().removeIf
		(node -> (((SearchTreeNode) node).getDepth() <= depth));

		// 
		return (Stack<SearchTreeNode>) dfSearch(problem);

	}

	/////////

	// 
	public static Stack<SearchTreeNode> idSearch(SearchProblem problem) {

		// 
		int maxDepth = 0;

		// 
		for(List<Object> firstLevelNode : problem.getStateSpace())
			for(Object secondLevelNode : firstLevelNode)
				if(((SearchTreeNode) secondLevelNode).getDepth() > maxDepth)
					maxDepth = ((SearchTreeNode) secondLevelNode).getDepth();

		// 
		return (Stack<SearchTreeNode>) idSearchHelper(problem, maxDepth);

	}

	// 
	private static Stack<SearchTreeNode> idSearchHelper(SearchProblem problem, int depth) {

		// 
		Stack<SearchTreeNode> base = dlSearch(problem, 0);
		Stack<SearchTreeNode> rest = idSearchHelper(problem, depth--);

		base.addAll(rest); // 

		return base; // 

	}

	/////////////////////////

	// 
	public static PriorityQueue<SearchTreeNode>
	bestFirstSearch(SearchProblem problem, QingFunction qing) {

		// 
		return (PriorityQueue<SearchTreeNode>) generalSearch(problem, qing);

	}

	/////////

	// 
	public static PriorityQueue<SearchTreeNode> greedySearch(SearchProblem problem) {

		// 
		return (PriorityQueue<SearchTreeNode>)
				bestFirstSearch(problem, QingFunction.BEST_FIRST_GREEDY);

	}

	/////////

	// 
	public static PriorityQueue<SearchTreeNode> aStarSearch(SearchProblem problem) {

		// 
		return (PriorityQueue<SearchTreeNode>)
				bestFirstSearch(problem, QingFunction.BEST_FIRST_ASTAR);

	}

	/////////////////////////

}