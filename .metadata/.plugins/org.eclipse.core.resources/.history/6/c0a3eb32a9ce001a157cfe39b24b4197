package search;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

import model.SearchProblem;
import model.SearchTreeNode;

// The general class that handles all types of search algorithms
public abstract class Search {

	///////////////////////// General Search /////////////////////////

	// The generic search algorithm, taking a problem and a search strategy as inputs.
	public static Collection<SearchTreeNode> generalSearch
	(SearchProblem problem, QingFunction qing) {

		// Declaring and initializing the expansion collection
		Collection<SearchTreeNode> nodes = destCreator(problem, qing);

		// Loop till you reach to the solution or return a failure
		while(!nodes.isEmpty()) {

			// Extract the first node from the collection
			SearchTreeNode node = nodeRemoval(nodes);

			// If this node is the goal state, congratulations!
			if (problem.isGoal((SearchTreeNode) node.getState())) {

				// Add it again at the front of the collection
				nodeAdder(nodes, node);
				
				return nodes; // Returns the expansion queue (with goal state at top)

			} else {

				// Wasn't the goal state ? expand it, put it back and try again!
				nodes = qinFun(nodes, expand(node, problem), qing);
			}
			
		}

		return nodes; // Returns a null collection (Failure)
	}

	///////////////////////// General Search - helper methods /////////////////////////

	// Helper method to initialize the expansion collection
	private static Collection<SearchTreeNode> destCreator(SearchProblem p, QingFunction q) {

		// Declare and initialize the output (null)
		Collection<SearchTreeNode> dest = null;

		// Based on the queuing method, initialize the output (null)
		switch(q) {
		case ENQUEUE_AT_END: dest = new LinkedList<SearchTreeNode>(); break;
		case ENQUEUE_AT_FRONT: dest = new Stack<SearchTreeNode>(); break;
		case ORDERED_INSERT_UCS: dest =
				new PriorityQueue<SearchTreeNode>(new NodeComparatorUCS()); break;
		case BEST_FIRST_GREEDY: dest =
				new PriorityQueue<SearchTreeNode>(new NodeComparatorGreedy1()); break;
		case BEST_FIRST_ASTAR: dest =
				new PriorityQueue<SearchTreeNode>(new NodeComparatorAStar()); break;
		default: dest = null;
		}

		// After initializing the output, add the initial state of the root as a start
		dest.add(new SearchTreeNode(p.getInitialState()));

		// Returns the resulting declared and initialized expansion collection
		return dest;

	}
	
	///////// nodeRemoval() helper method /////////

	// Helper method to extract the first node from the expansion collection
	private static SearchTreeNode nodeRemoval(Collection<SearchTreeNode> nodes) {

		// Declare and initialize the output (null)
		SearchTreeNode node = null;
		
		if(nodes instanceof Queue<?>) // (Either a linked list or a priority queue)
			node = ((Queue<SearchTreeNode>) nodes).poll(); // extract the first node
		if(nodes instanceof Stack<?>) // Check if it was a stack
			node = ((Stack<SearchTreeNode>) nodes).pop(); // extract the first node
		return node; // Returns the first node from the expansion collection

	}
	
	///////// nodeAdder() helper method /////////
	
	// Helper method to add a node back to the beginning of the expansion collection
	private static void nodeAdder(Collection<SearchTreeNode> nodes, SearchTreeNode n) {
		
		if(nodes instanceof LinkedList<?>) // Check if it was a linked list
			((LinkedList<SearchTreeNode>) nodes).add(0, n); // Add the node at front
		if(nodes instanceof PriorityQueue<?>) // Check if it was a priority queue
			((PriorityQueue<SearchTreeNode>) nodes).add(n); // Add the node at front
		if(nodes instanceof Stack<?>) // Check if it was a stack
			((Stack<SearchTreeNode>) nodes).push(n); // Add the node at front
		
	}
	
	///////// expand() helper method /////////

	// Helper method to extract child SearchTreeNodes
	private static Collection<SearchTreeNode> expand
	(SearchTreeNode node, SearchProblem problem) {

		// Declare and initialize the output
		Collection<SearchTreeNode> expanded = new ArrayList<SearchTreeNode>();
		
		// Loop through the state space to find child nodes
		for(List<Object>  firstLevel : problem.getStateSpace())
			for(Object secondLevel : firstLevel)
				if(((SearchTreeNode) secondLevel).getParent().equals(node))
					expanded.add((SearchTreeNode) secondLevel);
		
		return expanded; // Returns the expanded collection

	}
	
	///////// qinFun() helper method /////////

	// Helper method to queue expanded nodes into the expansion collection
	private static Collection<SearchTreeNode> qinFun
	(Collection<SearchTreeNode> dest, Collection<SearchTreeNode> src, QingFunction q) {

		// Based on the queuing method, add 'src' to the 'dest' collection
		switch(q) {
		case ENQUEUE_AT_END: dest.addAll(src); break;
		case ENQUEUE_AT_FRONT: for(Object e : src)
		{ ((Stack<SearchTreeNode>) dest).push((SearchTreeNode) e); } break;
		case ORDERED_INSERT_UCS: case BEST_FIRST_GREEDY: case BEST_FIRST_ASTAR:
			for(Object e : src) { dest.add((SearchTreeNode) e); } break;
		default: dest.add(null);
		}
		
		return dest; // Returns the resulting mix of 'src' and 'dest'

	}

	///////////////////////// Uninformed (Blind) Search Strategies /////////////////////////

	// Breadth-First Search: Expands all nodes of depth d before those of depth d + 1.
	public static LinkedList<SearchTreeNode> bfSearch(SearchProblem problem) {

		// Specifies the expansion collection as a normal queue
		return (LinkedList<SearchTreeNode>)
				generalSearch(problem, QingFunction.ENQUEUE_AT_END);

	}

	///////// ucSearch() search strategy /////////

	/* Uniform Cost Search: Expands nodes with lowest path cost first.
	 * Identical to BFS when path cost is identical to path length.
	 */
	public static PriorityQueue<SearchTreeNode> ucSearch(SearchProblem problem) {

		// Specifies the expansion collection as a priorityQueue queue (UCS Version)
		return (PriorityQueue<SearchTreeNode>)
				generalSearch(problem, QingFunction.ORDERED_INSERT_UCS);

	}

	///////// dfSearch() search strategy /////////

	/* Depth-First Search:
	 * Expands deeper nodes first.
	 * Backtracks if search hits a dead-end.
	 * Identical to UCS with operator costs of −1.
	 */
	public static Stack<SearchTreeNode> dfSearch(SearchProblem problem) {

		// Specifies the expansion collection as a stack
		return (Stack<SearchTreeNode>)
				generalSearch(problem, QingFunction.ENQUEUE_AT_FRONT);

	}

	///////// dlSearch() search strategy /////////

	/* Depth-Limited Search:
	 * Similar to depth-first search, but imposes a cut-off, l,
	 * on the maximum depth of a path.
	 */
	public static Stack<SearchTreeNode> dlSearch(SearchProblem problem, int depth) {

		// Filter the state space to satisfy the depth condition
		problem.getStateSpace().removeIf
		(node -> (((SearchTreeNode) node).getDepth() <= depth));

		// Passing the problem to the depth-first algorithm, after editing the state space
		return (Stack<SearchTreeNode>) dfSearch(problem);

	}

	///////// idSearch() search strategy /////////

	/* Iterative Deepening Search:
	 * Tries all depths, starting with 0.
	 */
	public static Stack<SearchTreeNode> idSearch(SearchProblem problem) {

		// Declaring and initializing a variable to keep track of the maximum depth so far
		int maxDepth = 0;

		// Loop through the poblem's state space to find the maximum depth
		for(List<Object> firstLevelNode : problem.getStateSpace())
			for(Object secondLevelNode : firstLevelNode)
				if(((SearchTreeNode) secondLevelNode).getDepth() > maxDepth)
					maxDepth = ((SearchTreeNode) secondLevelNode).getDepth();

		// Passing the problem to the helper recursive method
		return (Stack<SearchTreeNode>) idSearchHelper(problem, maxDepth);

	}

	// Helper method to recursively solve the problem using IDS
	private static Stack<SearchTreeNode> idSearchHelper(SearchProblem problem, int depth) {

		// Declaring and initializing the recursion base case and the recursion call
		Stack<SearchTreeNode> base = dlSearch(problem, 0);
		Stack<SearchTreeNode> rest = idSearchHelper(problem, depth--);
		
		base.addAll(rest); // Combining the parts of the result

		return base; // Returns the resulting combination

	}

	///////////////////////// Informed Search Strategies /////////////////////////

	/* Best-First Search:
	 * Class of search strategies that expands nodes in order of desirability
	 * as indicated by some evaluation function.
	 * Different definitions of the evaluation function
	 * give rise to several best-first strategies.
	 */
	public static PriorityQueue<SearchTreeNode>
	bestFirstSearch(SearchProblem problem, QingFunction qing) {

		// Passing the problem to the general search with the determined queuing method
		return (PriorityQueue<SearchTreeNode>) generalSearch(problem, qing);

	}

	///////// greedySearch() search strategy /////////

	/* Greedy Search:
	 * Minimizes the estimated cost to reach the goal.
	 * Uses a heuristic evaluation function h(n):
	 * h(n) = estimated cost of the cheapest path
	 * from the state at node n to a goal state.
	 */ 
	public static PriorityQueue<SearchTreeNode> greedySearch(SearchProblem problem) {

		// Specifies the expansion collection as a priorityQueue queue (Greedy Version)
		return (PriorityQueue<SearchTreeNode>)
				bestFirstSearch(problem, QingFunction.BEST_FIRST_GREEDY);

	}

	///////// aStarSearch() search strategy /////////

	/* A* Search:
	 * It combines the best of two worlds:
	 * the efficiency of greedy search
	 * and the “completeness and optimality” of uniform cost search.
	 * How? It defines the evaluation function as f(n) = g(n) + h(n)
	 */
	public static PriorityQueue<SearchTreeNode> aStarSearch(SearchProblem problem) {

		// Specifies the expansion collection as a priorityQueue queue (A* Version)
		return (PriorityQueue<SearchTreeNode>)
				bestFirstSearch(problem, QingFunction.BEST_FIRST_ASTAR);

	}

	/////////////////////////

}