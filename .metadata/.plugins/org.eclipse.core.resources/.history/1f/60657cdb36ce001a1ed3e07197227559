package model;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

// Subclass of the generic search problem.
public class StoneAgeRun extends SearchProblem {

	///////////////////////// Instance variables /////////////////////////
	
	private Collection<Object> operators; // Set of actions available to the agent.
	private StoneAgeRunState initialState, goalState; // Agent Start/Goal state.
	private List<List<Object>> stateSpace; // The set of states: initialState -> state.
	private Object[][] grid; // The problem's generated grid.

	///////// Constructor /////////
	
	// Problem constructor
	public StoneAgeRun()  { operatorsSetup(); GenGrid(); }

	///////// operatorsSetup() helper method /////////

	// Helper method to setup operators' ArrayList
	private void operatorsSetup() {

		operators = new ArrayList<Object>();
		operators.add(StoneAgeRunOperators.RIGHT);
		operators.add(StoneAgeRunOperators.MOVE);

	}
	
	///////////////////////// GenGrid() method /////////////////////////

	/* generates a random grid. The dimensions of the grid, the initial location
	of the agent in the second row, as well as the locations and orientations of the rocks
	are to be randomly generated. */
	private void GenGrid() {

		int min = 4, dimMax = 10,
			rand1 = (int) (Math.random() * dimMax) + min,
			rand2 = (int) (Math.random() * dimMax) + min,
			agentYRange = rand2 - min + 1,
			agentY = (int) (Math.random() * agentYRange);

		// Generating a random dimensions 2D grid
		grid = new Object[rand1][rand2];

		// Initial location of the agent in the second row
		initialState = new StoneAgeRunState(new Point(1, agentY), new Point(1, agentY + 1));

		// Goal state initialization
		goalState = new StoneAgeRunState(new Point(1, rand2 - 2), new Point(1, rand2 - 1));

		// Rocks generating
		rocksGenerator(rand1, rand2);

		// State space filling
		stateSpaceFiller(rand1, rand2);

	}
	
	///////// GenGrid() helper methods /////////

	// Helper method to setup the stateSpace's ArrayList 
	private void stateSpaceFiller(int rand1, int rand2) {

		stateSpace = new ArrayList<List<Object>>(rand2 - 1);
		for (int i = 0; i < rand2 - 2; i++) {

			stateSpace.add(new ArrayList<Object>());

			stateSpace.get(i).add
			(new StoneAgeRunState(new Point(1, i), new Point(1, i + 1)));

			stateSpace.get(i).add
			(new StoneAgeRunState(new Point(1, i + 1), new Point(1, i + 2)));

		}

		stateSpace.add(new ArrayList<Object>());

		stateSpace.get(stateSpace.size()-1).add
		(new StoneAgeRunState(new Point(1, rand2 - 2), new Point(1, rand2 - 1)));

	}

	// Helper method to generate rocks (random dimensions, locations and orientations)
	private void rocksGenerator(int rand1, int rand2) {

		RockOrientation rockOrientation;
		int rockSize, p1Max,
		nRocksMax = 2 , nRocks = (int) (Math.random() * nRocksMax) + 1;
		Point p1Cell, p2Cell, p3Cell;
		Rock rock;
		ArrayList<Rock> rocksArray = new ArrayList<Rock>();

		while(nRocks > 0) {

			rockOrientation = (int) (Math.random() * 2) == 0 ?
					RockOrientation.H : RockOrientation.V;
			
			rockSize = (int) (Math.random() * 2) + 2;
			
			p1Max = (rockOrientation == RockOrientation.H ?
					rand2 - (rockSize == 2 ? 2 : 3) : rand1 - (rockSize == 2 ? 2 : 3));
			
			p1Cell = new Point(rockOrientation == RockOrientation.H ?
					(int) Math.random() * rand1 : (int) Math.random() * p1Max,
					rockOrientation == RockOrientation.V ?
							(int) Math.random() * rand2 : (int) Math.random() * p1Max);
			
			p2Cell = rockOrientation == RockOrientation.H ?
					new Point(p1Cell.x, p1Cell.y + 1) : new Point(p1Cell.x + 1, p1Cell.y);
			
			p3Cell = rockSize == 3 ? rockOrientation == RockOrientation.H ?
					new Point(p1Cell.x, p1Cell.y + 2) :
						new Point(p1Cell.x + 2, p1Cell.y) : null;
			
			rock = rockSize == 2 ? new Rock(rockOrientation, p1Cell, p2Cell) :
				new Rock(rockOrientation, p1Cell, p2Cell, p3Cell);
			
			if (!rocksArray.contains(rock) && !isAgentConflict(rock)) {
				
				rocksArray.add(rock);
				nRocks--;
				gridRockMapper(rock);
				
			}
			
		}

	}
	
	// Helper method to detect if the generated rock conflicts agent's location
	private boolean isAgentConflict(Rock r) {
		
		if(r.getFirstCell().equals(getInitialState().getFirstCell()) ||
				r.getFirstCell().equals(getInitialState().getSecondCell()) ||
				r.getSecondCell().equals(getInitialState().getFirstCell()) ||
				r.getSecondCell().equals(getInitialState().getSecondCell()) ||
				r.getThirdCell().equals(getInitialState().getFirstCell()) ||
				r.getThirdCell().equals(getInitialState().getSecondCell()))
			return true;
		return false;
		
	}
	
	// Helper method to map a rock to the grid
	private void gridRockMapper(Rock r) {
		
		grid[r.getFirstCell().x][r.getFirstCell().y] = r;
		grid[r.getSecondCell().x][r.getSecondCell().y] = r;
		if(!r.getThirdCell().equals(null))
			grid[r.getFirstCell().x][r.getFirstCell().y] = r;
		
	}
	
	///////////////////////// Goal test & path cost methods /////////////////////////

	// Goal test function, applied to stop the agent when reaching it's goal
	public boolean isGoal(Object node) {

		if(node instanceof SearchTreeNode)
			if ((((SearchTreeNode) node).getState()).equals(this.getGoalState()))
				return true;
		return false;

	}

	// Path cost function, the sum of the costs of individual actions in the sequence.
	public double pathCost(SearchTreeNode node) { return node.getPathCost(); }

	///////////////////////// Getter methods /////////////////////////
	
	// Getter method for the set of actions available to the agent
	public Collection<Object> getOperators() { return operators; }

	// Getter method for the agent's start state
	public StoneAgeRunState getInitialState() { return initialState; }

	// Getter method for the agent's end state
	public StoneAgeRunState getGoalState() { return goalState; }

	// Getter method for the state space
	public List<List<Object>> getStateSpace() { return stateSpace; }

	// Getter method for the problem's generated grid
	public Object[][] getGrid() { return grid; }

	/////////////////////////
	
}