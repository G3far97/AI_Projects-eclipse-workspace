import java.util.ArrayList;
import java.util.Collection;

import model.SearchProblem;

public abstract class TreeSearch {

    Collection<SearchTreeNode> frontier;

    public SearchTreeNode solve(SearchProblem problem) {
        System.out.println("Solving...");
        frontier = initFrontier();
        frontier.addAll(expand(new SearchTreeNode(problem.getInitialState()), problem));
        System.out.println("Starting frontier is " + frontier);
        boolean done = false;
        SearchTreeNode solution = null;
        while (!done) {
            if (frontier.isEmpty()) {
                System.out.println("No more nodes in frontier => FAILURE");
                done = true;
            } else {
                SearchTreeNode node = chooseLeafNode(frontier, problem);
                System.out.println("Inspecting node " + node);
                if (problem.isGoal(node.getState())) {
                    System.out.println("Goal node reached => SUCCESS");
                    solution = node;
                    done = true;
                } else {
                    frontier.addAll(expand(node, problem));
                    System.out.printf("Expanding node, frontier is " + frontier);
                }
            }
        }
        return solution;
    }

    public Collection<SearchTreeNode> expand(SearchTreeNode node, SearchProblem problem) {
        Collection<SearchTreeNode> nodes = new ArrayList<SearchTreeNode>();
        Collection<Object> actions = problem.getOperators(node.getState());
        for (Object action : actions) {
            Object next = problem.getNextState(node.getState(), action);
            nodes.add(new SearchTreeNode(next, node, action, 
                problem.getStepCost(node.getState(), action, next)));
        }
        return nodes;
    }

    public abstract Collection<SearchTreeNode> initFrontier();

    public abstract SearchTreeNode chooseLeafNode(Collection<SearchTreeNode> frontier, SearchProblem problem);
}