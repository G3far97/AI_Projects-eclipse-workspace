package search;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

import model.SearchProblem;
import model.SearchTreeNode;

// 
public abstract class Search {

	///////////////////////// General Search /////////////////////////

	// The generic search algorithm, taking a problem and a search strategy as inputs.
	public static Collection<SearchTreeNode> generalSearch
	(SearchProblem problem, QingFunction qing) {

		// Declaring and initializing the expansion storage collection
		Collection<SearchTreeNode> nodes = destCreator(problem, qing);

		// Loop till you reach to the solution or return a failure
		while(!nodes.isEmpty()) {

			// Extract the first node from the storage collection
			SearchTreeNode node = nodeRemoval(nodes);

			// If this node is the goal state, congratulations!
			if (problem.isGoal((SearchTreeNode) node.getState())) {

				// Add it again at the front of the storage collection
				nodeAdder(nodes, node);
				
				return nodes; // Returns the expansion queue (with goal state at top)

			} else {

				// Wasn't the goal state ? expand it, put it back and try again!
				nodes = qinFun(nodes, expand(node, problem.getOperators()), qing);
			}
		}

		return nodes; // Returns a null collection (Failure)
	}

	///////////////////////// General Search - helper methods /////////////////////////

	// Helper method to initialize the expansion storage collection
	private static Collection<SearchTreeNode> destCreator(SearchProblem p, QingFunction q) {

		// Declare and initialize the output (null)
		Collection<SearchTreeNode> dest = null;

		// Based on the queuing method, initialize the output (null)
		switch(q) {
		case ENQUEUE_AT_END: dest = new LinkedList<SearchTreeNode>(); break;
		case ENQUEUE_AT_FRONT: dest = new Stack<SearchTreeNode>(); break;
		case ORDERED_INSERT_UCS: dest =
				new PriorityQueue<SearchTreeNode>(new NodeComparatorUCS()); break;
		case BEST_FIRST_GREEDY: dest =
				new PriorityQueue<SearchTreeNode>(new NodeComparatorGreedy()); break;
		case BEST_FIRST_ASTAR: dest =
				new PriorityQueue<SearchTreeNode>(new NodeComparatorAStar()); break;
		default: dest = null;
		}

		// After initializing the output, add the initial state of the root as a start
		dest.add(new SearchTreeNode(p.getInitialState()));

		// Returns the resulted declared and initialized expansion storage collection
		return dest;

	}
	
	/////////

	// Helper method to extract the first node from the expansion storage collection
	private static SearchTreeNode nodeRemoval(Collection<SearchTreeNode> nodes) {

		// Declare and initialize the output (null)
		SearchTreeNode node = null;
		
		if(nodes instanceof Queue<?>) // (Either a linked list or a priority queue)
			node = ((Queue<SearchTreeNode>) nodes).poll(); // extract the first node
		if(nodes instanceof Stack<?>) // Check if it was a stack
			node = ((Stack<SearchTreeNode>) nodes).pop(); // extract the first node
		return node; // Returns the first node from the expansion storage collection

	}
	
	/////////
	
	// Helper method to add a node back to the beginning of the expansion storage collection
	private static void nodeAdder(Collection<SearchTreeNode> nodes, SearchTreeNode n) {
		
		if(nodes instanceof LinkedList<?>) // Check if it was a linked list
			((LinkedList<SearchTreeNode>) nodes).add(0, n); // Add the node at front
		if(nodes instanceof PriorityQueue<?>) // Check if it was a priority queue
			((PriorityQueue<SearchTreeNode>) nodes).add(n); // Add the node at front
		if(nodes instanceof Stack<?>) // Check if it was a stack
			((Stack<SearchTreeNode>) nodes).push(n); // Add the node at front
		
	}
	
	/////////

	// Helper method to extract child SearchTreeNodes
	private static Collection<SearchTreeNode> expand
	(SearchTreeNode node, Collection<Object> operators) {

		// Declare and initialize the output
		Collection<SearchTreeNode> expanded = new ArrayList<SearchTreeNode>();

		// 
		//for(Object o : operators) {  }
		
		return expanded; // Returns the expanded collection

	}
	
	/////////

	// Helper method to queue expanded nodes into the expansion storage
	private static Collection<SearchTreeNode> qinFun
	(Collection<SearchTreeNode> dest, Collection<SearchTreeNode> src, QingFunction q) {

		// Based on the queuing method, add 'src' to the 'dest' collection
		switch(q) {
		case ENQUEUE_AT_END: dest.addAll(src); break;
		case ENQUEUE_AT_FRONT: for(Object e : src)
		{ ((Stack<SearchTreeNode>) dest).push((SearchTreeNode) e); } break;
		case ORDERED_INSERT_UCS:
		case BEST_FIRST_GREEDY:
		case BEST_FIRST_ASTAR: for(Object e : src) { dest.add((SearchTreeNode) e); } break;
		default: dest.add(null);
		}
		
		return dest; // Returns the resulted mix of 'src' and 'dest'

	}

	///////////////////////// Uninformed Search Strategies /////////////////////////

	// 
	public static LinkedList<SearchTreeNode> bfSearch(SearchProblem problem) {

		// 
		return (LinkedList<SearchTreeNode>)
				generalSearch(problem, QingFunction.ENQUEUE_AT_END);

	}

	/////////

	// 
	public static PriorityQueue<SearchTreeNode> ucSearch(SearchProblem problem) {

		// 
		return (PriorityQueue<SearchTreeNode>)
				generalSearch(problem, QingFunction.ORDERED_INSERT_UCS);

	}

	/////////

	// 
	public static Stack<SearchTreeNode> dfSearch(SearchProblem problem) {

		// 
		return (Stack<SearchTreeNode>)
				generalSearch(problem, QingFunction.ENQUEUE_AT_FRONT);

	}

	/////////

	// 
	public static Stack<SearchTreeNode> dlSearch(SearchProblem problem, int depth) {

		// 
		problem.getStateSpace().removeIf
		(node -> (((SearchTreeNode) node).getDepth() <= depth));

		// 
		return (Stack<SearchTreeNode>) dfSearch(problem);

	}

	/////////

	// 
	public static Stack<SearchTreeNode> idSearch(SearchProblem problem) {

		// 
		int maxDepth = 0;

		// 
		for(List<Object> firstLevelNode : problem.getStateSpace())
			for(Object secondLevelNode : firstLevelNode)
				if(((SearchTreeNode) secondLevelNode).getDepth() > maxDepth)
					maxDepth = ((SearchTreeNode) secondLevelNode).getDepth();

		// 
		return (Stack<SearchTreeNode>) idSearchHelper(problem, maxDepth);

	}

	// 
	private static Stack<SearchTreeNode> idSearchHelper(SearchProblem problem, int depth) {

		// 
		Stack<SearchTreeNode> base = dlSearch(problem, 0);
		Stack<SearchTreeNode> rest = idSearchHelper(problem, depth--);

		base.addAll(rest); // 

		return base; // 

	}

	///////////////////////// Informed Search Strategies /////////////////////////

	// 
	public static PriorityQueue<SearchTreeNode>
	bestFirstSearch(SearchProblem problem, QingFunction qing) {

		// 
		return (PriorityQueue<SearchTreeNode>) generalSearch(problem, qing);

	}

	/////////

	// 
	public static PriorityQueue<SearchTreeNode> greedySearch(SearchProblem problem) {

		// 
		return (PriorityQueue<SearchTreeNode>)
				bestFirstSearch(problem, QingFunction.BEST_FIRST_GREEDY);

	}

	/////////

	// 
	public static PriorityQueue<SearchTreeNode> aStarSearch(SearchProblem problem) {

		// 
		return (PriorityQueue<SearchTreeNode>)
				bestFirstSearch(problem, QingFunction.BEST_FIRST_ASTAR);

	}

	/////////////////////////

}